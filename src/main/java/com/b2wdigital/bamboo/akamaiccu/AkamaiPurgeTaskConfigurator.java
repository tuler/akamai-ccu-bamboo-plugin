package com.b2wdigital.bamboo.akamaiccu;

import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;

public class AkamaiPurgeTaskConfigurator extends AbstractTaskConfigurator {

	public static final String CONFIG_USERNAME = "username";
	public static final String CONFIG_PASSWORD = "password";
	public static final String CONFIG_TYPE = "type";
	public static final String CONFIG_ACTION = "action";
	public static final String CONFIG_DOMAIN = "domain";
	public static final String CONFIG_OBJECTS = "objects";
	public static final String CONFIG_OBJECTS_FILE = "objects_file";
	public static final String CONFIG_WAIT_FOR_COMPLETION = "waitForCompletion";

	private TextProvider textProvider;

	@Override
	public void populateContextForCreate(Map<String, Object> context) {
		super.populateContextForCreate(context);
		PurgeRequest defaultRequest = new PurgeRequest();
		context.put(CONFIG_TYPE, defaultRequest.getType().name());
		context.put(CONFIG_ACTION, defaultRequest.getAction().name());
		context.put(CONFIG_DOMAIN, defaultRequest.getDomain().name());
		context.put(CONFIG_WAIT_FOR_COMPLETION, false);
	}

	@Override
	public void populateContextForEdit(Map<String, Object> context,
			TaskDefinition taskDefinition) {
		super.populateContextForEdit(context, taskDefinition);
		Map<String, String> config = taskDefinition.getConfiguration();
		context.put(CONFIG_USERNAME, config.get(CONFIG_USERNAME));
		context.put(CONFIG_PASSWORD, config.get(CONFIG_PASSWORD));
		context.put(CONFIG_TYPE, config.get(CONFIG_TYPE));
		context.put(CONFIG_ACTION, config.get(CONFIG_ACTION));
		context.put(CONFIG_DOMAIN, config.get(CONFIG_DOMAIN));
		context.put(CONFIG_OBJECTS, config.get(CONFIG_OBJECTS));
		context.put(CONFIG_OBJECTS_FILE, config.get(CONFIG_OBJECTS_FILE));
		context.put(CONFIG_WAIT_FOR_COMPLETION,
				config.get(CONFIG_WAIT_FOR_COMPLETION));
	}

	@Override
	public void populateContextForView(Map<String, Object> context,
			TaskDefinition taskDefinition) {
		Map<String, String> config = taskDefinition.getConfiguration();
		context.put(CONFIG_USERNAME, config.get(CONFIG_USERNAME));
		context.put(CONFIG_PASSWORD, config.get(CONFIG_PASSWORD));
		context.put(CONFIG_TYPE, config.get(CONFIG_TYPE));
		context.put(CONFIG_ACTION, config.get(CONFIG_ACTION));
		context.put(CONFIG_DOMAIN, config.get(CONFIG_DOMAIN));
		context.put(CONFIG_OBJECTS, config.get(CONFIG_OBJECTS));
		context.put(CONFIG_OBJECTS_FILE, config.get(CONFIG_OBJECTS_FILE));
		context.put(CONFIG_WAIT_FOR_COMPLETION,
				config.get(CONFIG_WAIT_FOR_COMPLETION));
	}

	@Override
	public Map<String, String> generateTaskConfigMap(
			ActionParametersMap params, TaskDefinition previousTaskDefinition) {
		final Map<String, String> config = super.generateTaskConfigMap(params,
				previousTaskDefinition);
		config.put(CONFIG_USERNAME, params.getString(CONFIG_USERNAME));
		config.put(CONFIG_PASSWORD, params.getString(CONFIG_PASSWORD));
		config.put(CONFIG_TYPE, params.getString(CONFIG_TYPE));
		config.put(CONFIG_ACTION, params.getString(CONFIG_ACTION));
		config.put(CONFIG_DOMAIN, params.getString(CONFIG_DOMAIN));
		config.put(CONFIG_OBJECTS, params.getString(CONFIG_OBJECTS));
		config.put(CONFIG_OBJECTS_FILE, params.getString(CONFIG_OBJECTS_FILE));
		config.put(CONFIG_WAIT_FOR_COMPLETION,
				params.getString(CONFIG_WAIT_FOR_COMPLETION));
		return config;
	}

	@Override
	public void validate(ActionParametersMap params,
			ErrorCollection errorCollection) {
		super.validate(params, errorCollection);
		final String username = params.getString(CONFIG_USERNAME);
		if (StringUtils.isEmpty(username)) {
			errorCollection.addError(
					CONFIG_USERNAME,
					textProvider.getText("akamaiccu." + CONFIG_USERNAME
							+ ".error"));
		}
		final String password = params.getString(CONFIG_PASSWORD);
		if (StringUtils.isEmpty(password)) {
			errorCollection.addError(
					CONFIG_PASSWORD,
					textProvider.getText("akamaiccu." + CONFIG_PASSWORD
							+ ".error"));
		}
		final String objects = params.getString(CONFIG_OBJECTS);
		if (StringUtils.isEmpty(objects)) {
			errorCollection.addError(
					CONFIG_OBJECTS,
					textProvider.getText("akamaiccu." + CONFIG_OBJECTS
							+ ".error"));
		}
	}

	public void setTextProvider(final TextProvider textProvider) {
		this.textProvider = textProvider;
	}
}
