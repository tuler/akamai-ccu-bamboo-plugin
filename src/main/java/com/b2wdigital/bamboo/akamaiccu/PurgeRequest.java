package com.b2wdigital.bamboo.akamaiccu;

import java.util.ArrayList;
import java.util.List;

public class PurgeRequest {

	private PurgeRequestType type;
	
	private PurgeRequestAction action;
	
	private PurgeRequestDomain domain;
	
	private List<String> objects;

	public PurgeRequest() {
		type = PurgeRequestType.arl;
		action = PurgeRequestAction.remove;
		domain = PurgeRequestDomain.production;
		objects = new ArrayList<String>();
	}
	
	public PurgeRequestType getType() {
		return type;
	}

	public void setType(PurgeRequestType type) {
		this.type = type;
	}

	public List<String> getObjects() {
		return objects;
	}

	public void setObjects(List<String> objects) {
		this.objects = objects;
	}
	
	public void addObject(String object) {
		this.objects.add(object);
	}
	
	public String toJson() {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		sb.append("\"type\":\"").append(this.type.name()).append("\",");
		sb.append("\"objects\":[");
		for (int i = 0; i < this.objects.size(); i++) {
			sb.append("\"").append(this.objects.get(i)).append("\"");
			if (i < this.objects.size() - 1) {
				sb.append(',');
			}
		}
		sb.append("]}");
		return sb.toString();
	}

	public PurgeRequestAction getAction() {
		return action;
	}

	public void setAction(PurgeRequestAction action) {
		this.action = action;
	}

	public PurgeRequestDomain getDomain() {
		return domain;
	}

	public void setDomain(PurgeRequestDomain domain) {
		this.domain = domain;
	}
}
