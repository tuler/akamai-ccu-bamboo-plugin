package com.b2wdigital.bamboo.akamaiccu;

public enum PurgeRequestAction {
	remove, invalidate
}
