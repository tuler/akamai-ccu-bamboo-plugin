package com.b2wdigital.bamboo.akamaiccu;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

public class AkamaiPurgeTask implements CommonTaskType {

	private static int MAX_QUERY_COUNT = 5;

	@Override
	public TaskResult execute(CommonTaskContext taskContext) throws TaskException {
		final TaskResultBuilder builder = TaskResultBuilder
				.newBuilder(taskContext);
		final BuildLogger buildLogger = taskContext.getBuildLogger();
		final ConfigurationMap config = taskContext.getConfigurationMap();
		final String username = config
				.get(AkamaiPurgeTaskConfigurator.CONFIG_USERNAME);
		final String password = config
				.get(AkamaiPurgeTaskConfigurator.CONFIG_PASSWORD);

		PurgeRequest request = new PurgeRequest();
		request.setType(PurgeRequestType.valueOf(getOrDefault(
				config.get(AkamaiPurgeTaskConfigurator.CONFIG_TYPE),
				PurgeRequestType.arl.name())));
		request.setAction(PurgeRequestAction.valueOf(getOrDefault(
				config.get(AkamaiPurgeTaskConfigurator.CONFIG_ACTION),
				PurgeRequestAction.remove.name())));
		request.setDomain(PurgeRequestDomain.valueOf(getOrDefault(
				config.get(AkamaiPurgeTaskConfigurator.CONFIG_DOMAIN),
				PurgeRequestDomain.production.name())));
		request.setObjects(Arrays
				.asList(config.get("objects").split("\\r?\\n")));

		if (false) {
			try {
				String pathname = config
						.get(AkamaiPurgeTaskConfigurator.CONFIG_OBJECTS_FILE);
				List<String> objects = FileUtils.readLines(new File(pathname));
				request.setObjects(objects);
			} catch (IOException e1) {
				buildLogger.addErrorLogEntry(e1.getMessage(), e1);
				return builder.failedWithError().build();
			}
		}

		try {
			HttpResponse<String> response = Unirest
					.post("https://api.ccu.akamai.com/ccu/v2/queues/default")
					.header("Content-Type", "application/json")
					.basicAuth(username, password).body(request.toJson())
					.asString();

			if (response.getStatus() < 200 || response.getStatus() >= 300) {
				// did not return ok
				buildLogger.addErrorLogEntry(String.format("%d - %s",
						response.getStatus(), response.getStatusText()));
				buildLogger.addErrorLogEntry(response.getBody());
				return builder.failedWithError().build();
			}

			// parse response
			Gson gson = new Gson();
			PurgeResponse purgeResponse = gson.fromJson(response.getBody(),
					PurgeResponse.class);

			if (purgeResponse.getHttpStatus() == 201) {
				// log response
				buildLogger.addBuildLogEntry(response.getBody());

				boolean waitForCompletion = taskContext
						.getConfigurationMap()
						.getAsBoolean(
								AkamaiPurgeTaskConfigurator.CONFIG_WAIT_FOR_COMPLETION);
				if (waitForCompletion) {
					// keeps polling CCU until completion
					return wait(purgeResponse, username, password, builder,
							buildLogger);
				} else {
					// return success
					return builder.success().build();
				}
			} else {
				// log error
				buildLogger.addErrorLogEntry(response.getBody());
				return builder.failed().build();
			}

		} catch (UnirestException e) {
			buildLogger.addErrorLogEntry(e.getMessage(), e);
			return builder.failedWithError().build();
		} catch (JsonSyntaxException e2) {
			buildLogger.addErrorLogEntry(e2.getMessage(), e2);
			return builder.failedWithError().build();
		}
	}

	private String getOrDefault(String value, String defaultValue) {
		if (StringUtils.isNotBlank(value)) {
			return value;
		}
		return defaultValue;
	}

	private TaskResult wait(PurgeResponse purgeResponse, String username,
			String password, TaskResultBuilder builder, BuildLogger buildLogger)
			throws UnirestException {
		int queryCount = 0;
		Gson gson = new Gson();

		// keeps polling until done
		while (!"Done".equals(purgeResponse.getPurgeStatus())) {

			if (purgeResponse.getHttpStatus() < 200
					|| purgeResponse.getHttpStatus() >= 300) {
				// did not created ok, return error
				return builder.failed().build();
			}

			if (queryCount > MAX_QUERY_COUNT) {
				// check if we queried too many times, abort if yes
				buildLogger.addErrorLogEntry(String.format(
						"Aborting wait, queried %d times already", queryCount));
				return builder.failed().build();
			}

			try {
				int secondsToWait = purgeResponse.getPingAfterSeconds();
				buildLogger.addBuildLogEntry(String.format(
						"Waiting for %d seconds...", secondsToWait));
				Thread.sleep(secondsToWait * 1000);

				// check purge status
				HttpResponse<String> response = Unirest
						.get("https://api.ccu.akamai.com"
								+ purgeResponse.getProgressUri())
						.header("Content-Type", "application/json")
						.basicAuth(username, password).asString();

				if (response.getStatus() != 200) {
					buildLogger.addErrorLogEntry(String.format("%d - %s",
							response.getStatus(), response.getStatusText()));
					buildLogger.addErrorLogEntry(response.getBody());
					return builder.failedWithError().build();
				}

				// log
				buildLogger.addBuildLogEntry(response.getBody());
				
				// parse result
				purgeResponse = gson.fromJson(response.getBody(),
						PurgeResponse.class);
				queryCount++;

			} catch (InterruptedException e) {
				buildLogger.addErrorLogEntry("Wait interrupted", e);
				return builder.failedWithError().build();
			}
		}
		buildLogger.addBuildLogEntry(purgeResponse.getPurgeStatus());
		return builder.success().build();
	}

}
