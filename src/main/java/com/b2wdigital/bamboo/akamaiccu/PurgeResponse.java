package com.b2wdigital.bamboo.akamaiccu;

import java.util.Date;

public class PurgeResponse {

	private int httpStatus;

	private String detail;

	private int estimatedSeconds;

	private String purgeId;

	private String progressUri;

	private int pingAfterSeconds;

	private String supportId;

	private String title;

	private String describedBy;

	private int originalEstimatedSeconds;

	private int originalQueueLength;

	private Date completionTime;

	private Date submissionTime;

	private String purgeStatus;

	private String submittedBy;

	private int queueLength;

	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getEstimatedSeconds() {
		return estimatedSeconds;
	}

	public void setEstimatedSeconds(int estimatedSeconds) {
		this.estimatedSeconds = estimatedSeconds;
	}

	public String getPurgeId() {
		return purgeId;
	}

	public void setPurgeId(String purgeId) {
		this.purgeId = purgeId;
	}

	public String getProgressUri() {
		return progressUri;
	}

	public void setProgressUri(String progressUri) {
		this.progressUri = progressUri;
	}

	public int getPingAfterSeconds() {
		return pingAfterSeconds;
	}

	public void setPingAfterSeconds(int pingAfterSeconds) {
		this.pingAfterSeconds = pingAfterSeconds;
	}

	public String getSupportId() {
		return supportId;
	}

	public void setSupportId(String supportId) {
		this.supportId = supportId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescribedBy() {
		return describedBy;
	}

	public void setDescribedBy(String describedBy) {
		this.describedBy = describedBy;
	}

	public int getOriginalEstimatedSeconds() {
		return originalEstimatedSeconds;
	}

	public void setOriginalEstimatedSeconds(int originalEstimatedSeconds) {
		this.originalEstimatedSeconds = originalEstimatedSeconds;
	}

	public int getOriginalQueueLength() {
		return originalQueueLength;
	}

	public void setOriginalQueueLength(int originalQueueLength) {
		this.originalQueueLength = originalQueueLength;
	}

	public Date getCompletionTime() {
		return completionTime;
	}

	public void setCompletionTime(Date completionTime) {
		this.completionTime = completionTime;
	}

	public Date getSubmissionTime() {
		return submissionTime;
	}

	public void setSubmissionTime(Date submissionTime) {
		this.submissionTime = submissionTime;
	}

	public String getPurgeStatus() {
		return purgeStatus;
	}

	public void setPurgeStatus(String purgeStatus) {
		this.purgeStatus = purgeStatus;
	}

	public String getSubmittedBy() {
		return submittedBy;
	}

	public void setSubmittedBy(String submittedBy) {
		this.submittedBy = submittedBy;
	}

	public int getQueueLength() {
		return queueLength;
	}

	public void setQueueLength(int queueLength) {
		this.queueLength = queueLength;
	}
}
